#!/bin/sh
#
# Remove wrapper script/links in $FSLDIR/bin/ which invoke commands that
# are installed in $FSLDIR/fslpython/envs/fslpython/bin/.

# Only unlink if scripts are in subdirectory of FSLDIR
if [ ! -z ${FSLDIR} ]; then
  case "$PREFIX" in "${FSLDIR}"*)
    for script in $@; do
      targetScript="${FSLDIR}/bin/$script"

      # we use -e and not -f or -L,
      # because the script may be a
      # symlink in older versions
      # of FSL, or a wrapper script
      # in newer versions
     if [ -e $targetScript ]; then
       rm "$targetScript"
     fi
    done
  esac
fi
