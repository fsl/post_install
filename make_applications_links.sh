#!/usr/bin/env bash
# Script to copy macOS GUI .app bundles into /Applications
#
# Call with -f <FSLDIR path>, e.g. /usr/local/fsl (with use FSLDIR if given
# no arguments)
#
# As FSL .app bundles are copied, they should not contain a large amount of
# data. Ideally they should just contain a wrapper script which invokes the
# relevant command in $FSLDIR/bin.

arch=`uname -s`
if [ "${arch}" != "Darwin" ]; then
    exit 0
fi

# Where is this script?
script_dir=$( cd $(dirname $0) ; pwd)

# Set some defaults
OPTIND=1
fsl_dir=""
quiet=0
app_list="fslpython/envs/fslpython/bin/FSLeyes.app"

function syntax {
    echo "make_application_links.sh [-f <FSLDIR>] [-q]"
    echo "  -f <FSLDIR> Location of installed FSL, e.g. /usr/local/fsl"
    echo "                  if not provided looks for FSLDIR in environment"
    echo "  -q          Don't report progress'"
}

while getopts "h?qf:" opt; do
    case "${opt}" in
    h|\?)
        syntax
        exit 0
        ;;
    q)  quiet=1
        ;;
    f)  fsl_dir=${OPTARG}
        ;;
    esac
done

shift $((OPTIND-1))

[ "$1" = "--" ] && shift

if [ -z "${fsl_dir}" ]; then
    if [ -z "${FSLDIR}" ]; then
        echo "Error - FSLDIR not given as an argument and \$FSLDIR not set!" >&2
        exit 1
    else
        fsl_dir=${FSLDIR}
    fi
fi

if [ ! -e "${fsl_dir}/bin" ]; then
    echo "Error - ${fsl_dir}/bin does not exist!" >&2
    exit 1
fi

if [ $quiet -eq 0 ]; then
    echo "Creating links to applications."
fi

if [ ! -z "${SUDO_UID}" ]; then
    su_do=''
else
    su_do="sudo"
fi

echo $app_list
for app in ${app_list}; do
    target="${fsl_dir}/${app}"
    if [ ! -e "${target}" ]; then
        continue
    fi
    # if not running as root (e.g. sudo not used to install FSL)
    if [ $EUID -ne 0 ]; then
      # set link to user HOME/Applications (does not require admin password)
      # this is effecticly the default install location on macOS since most installs prob
      # won't use sudo
      link="${HOME}/Applications/`basename ${app}`"
      su_do='' # not running as root
    else
      link="/Applications/`basename ${app}`"
    fi
    # need to check both install conditions
    if [ "${link}" = "/Applications/" ] || [ "${link}" = "${HOME}/Applications/" ]; then
        continue
    fi
    if [ -L ${link} ]; then
        if [ ! "`readlink ${link}`" = "${target}" ]; then
            ${su_do} rm -f "${link}"
        else
            continue
        fi
    elif [ -e "${link}" ]; then
        echo "${link} exists and isn't a symbolic link - not changing"
        continue
    fi

    ${su_do} cp -r "${target}" "${link}"
done
