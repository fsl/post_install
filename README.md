# Project: post_install

The post install project is run after FSL is downloaded and placed in the correct directory (and configured)

## Steps

1) Install fslpython (fslpython_install.sh)

3) If Linux, configure eddy (configure_eddy.sh). If Darwin (macOS), make application links (make_application_links.sh) 