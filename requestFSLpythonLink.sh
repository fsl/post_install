#!/bin/sh
#
# Create wrapper scripts in $FSLDIR/bin/ which invoke commands that are
# installed in $FSLDIR/fslpython/envs/fslpython/bin/.

# Early versions of this script would create symlinks from $FSLDIR/bin/
# into $FSLDIR/fslpython/envs/fslpython/bin/. However, wrapper scripts
# are now used to work around a couple of problems:
#
#  - We need python scripts to exclusively use the libraries installed
#    in the fslpython conda environment. Users may have other Python
#    environments activated, and/or libraries installed into a local
#    site-packages directory. So we need to invoke the python interpreter
#    in isolated mode, with the -I flag:
#
#        https://docs.python.org/3/using/cmdline.html#id2
#
#  -  There is no guarantee that a shell supports shebang lines longer
#     than 127 characters. Depending on where FSL is installed, it may
#     not be possible to have a shebang line pointing to
#     $FSLDIR/fslpython/envs/fslpython/bin/python which does not exceed
#     127 characters.

# Wrapper script template - explicitly use the
# fslpython python interpreter in isolated mode.
scriptTemplate="#!/bin/sh
$FSLDIR/fslpython/envs/fslpython/bin/python -I"

# We search for these strings in source file
# headers to ID them as python scripts. We need
# to check both match "#!/usr/bin/env python"
# and  "#!<fsldir>/fslpython/envs/...",
# because conda might generate one or the other
# in different scenarios.
scriptIdentifier1="#!/usr/bin/env python"
scriptIdentifier2="/fslpython/envs/fslpython/bin/python"

# Only link if scripts are in subdirectory of FSLDIR
if [ ! -z ${FSLDIR} ]; then
  case "$PREFIX" in "${FSLDIR}"*)
    for script in $@; do
      sourceScript="${PREFIX}/bin/$script"
      if [ -f $sourceScript ]; then

        # Only create wrapper scripts for python
        # scripts - not for binaries, or scripts
        # in other languages
        id1=$(head -c 1024 "$sourceScript" | grep "$scriptIdentifier1")
        id2=$(head -c 1024 "$sourceScript" | grep "$scriptIdentifier2")
        if [ -z "$id1" ] && [ -z "$id2" ]; then
          continue
        fi

        # remove target script if it already exists
        targetScript="${FSLDIR}/bin/$script"
        if [ -e $targetScript ]; then
          rm "$targetScript"
        fi

        # create the wrapper script. We copy the
        # source script, and then overwrite it,
        # so file permissions are preserved.
        cp "$sourceScript" "$targetScript"
        echo "$scriptTemplate $sourceScript "'"$@"' > "$targetScript"
      fi
    done
  esac
fi
