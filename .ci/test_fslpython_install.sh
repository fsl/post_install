#!/usr/bin/env bash

set -e

# If we are running in a docker container,
# we are running the linux check. Otherwise
# we are probably running the macOS check.
if [ -f /.dockerenv ]; then
  yum -y check-update                       || true
  yum install -y which wget file mesa-libGL || true
fi

mkdir -p ./usr/local/fsl/bin
./fslpython_install.sh -d -f ./usr/local/fsl && echo "0" > status || echo "1" > status || true
status=`cat status`

if [ -z ${TMPDIR+x} ]; then
  TMPDIR=/tmp/
fi

logfile=$(ls -t ${TMPDIR}/fslpython*/fslpython_miniconda_installer.log|head -n1)

cat ${logfile}

if [[ "${status}" == "1" ]]; then
  exit 1
fi

source ./usr/local/fsl/fslpython/bin/activate fslpython
which python
conda list
python -c "import fsl.version as fv; print(fv.__version__)"
